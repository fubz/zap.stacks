import { PrismaClient } from '@prisma/client';
const prisma = new PrismaClient();

async function main() {
	const test = await prisma.authUser.upsert({
		where: { email: 'test@test.com' },
		update: {},
		create: {
			id: 'test1',
			email: 'test@test.com',
			firstName: 'Test',
			lastName: 'Stacks',
			role: 'USER'
		}
	});
	const admin = await prisma.authUser.upsert({
		where: { email: 'admin@test.com' },
		update: {},
		create: {
			id: 'a1',
			email: 'admin@test.com',
			firstName: 'Admin',
			lastName: 'Stacks',
			role: 'ADMIN'
		}
	});

	console.log({ test, admin });
}
main()
	.then(async () => {
		await prisma.$disconnect();
	})
	.catch(async (e) => {
		console.error(e);
		await prisma.$disconnect();
		process.exit(1);
	});
