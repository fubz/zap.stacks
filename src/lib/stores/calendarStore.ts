import { useWritable } from '$lib/_helpers/useSharedStore';

export const calendar = new Calendar(calendarElement, options);

export const calendarStore = useWritable('calendar', calendar);
