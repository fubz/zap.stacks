import { dev } from '$app/environment';
export const BASE_URL = dev ? 'http://localhost:5173' : 'https://zapstacker.fubz.dev';
export const APP_NAME = 'Zap.Stacker';
export const CONTACT_EMAIL = 'zapstacker@fubz.dev';
export const DOMAIN = 'zapstacker.fubz.dev';
/* WARNING!!! TERMS AND CONDITIONS AND PRIVACY POLICY 
WERE CREATED BY CHATGPT AS AN EXAMPLE ONLY. 
CONSULT A LAWYER AND DEVELOP YOUR OWN TERMS AND PRIVACY POLICY!!! */
export const TERMS_PRIVACY_CONTACT_EMAIL = 'zapstacker@fubz.dev';
export const TERMS_PRIVACY_WEBSITE = 'zapstacker.fubz.dev';
export const TERMS_PRIVACY_COMPANY = 'Fubz.Dev';
export const TERMS_PRIVACY_EFFECTIVE_DATE = 'January 1, 2023';
export const TERMS_PRIVACY_APP_NAME = 'Zap Stacker';
export const TERMS_PRIVACY_APP_PRICING_AND_SUBSCRIPTIONS =
	'[Details about the pricing, subscription model, refund policy]';
export const TERMS_PRIVACY_COUNTRY = 'United States';
