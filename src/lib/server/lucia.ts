// lib/server/lucia.ts
import { lucia } from 'lucia'
import { sveltekit } from 'lucia/middleware'
import { prisma as prismaAdapter } from '@lucia-auth/adapter-prisma'
import prisma from '../config/prisma'
import { dev } from '$app/environment'
import type { UserSchema } from '$lib/config/zod-schemas'

export const auth = lucia({
  adapter: prismaAdapter(prisma, { user: 'authUser', key: 'authKey', session: 'authSession' }),
  env: dev ? 'DEV' : 'PROD',
  middleware: sveltekit(),
  getUserAttributes: (userData) => {
    console.log('lucia', { userData })

    return {
      userId: userData.id,
      email: userData.email,
      firstName: userData.firstName,
      lastName: userData.lastName,
      role: userData.role,
      verified: userData.verified,
      token: userData.token,
      // groups: userData.groups.map((group) => ({
      //   groupId: group.groupId,
      //   userId: group.userId,
      //   role: group.role,
      // })),
    }
  },
  sessionCookie: {
    name: 'stack_session',
    attributes: {
      sameSite: 'lax',
      path: '/',
    },
  },
})

export type Auth = typeof auth

export async function signin({ email, password }: Pick<UserSchema, 'email' | 'password'>) {
  return auth.useKey('email', email, password)
}
