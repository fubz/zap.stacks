// src/app.d.ts
/// <reference types="lucia" />

import type { AuthUser } from '@prisma/client'

declare global {
  namespace Lucia {
    type Auth = import('$lib/server/lucia').Auth
    type DatabaseUserAttributes = {
      userId: string
      email: string
      firstName: string
      lastName: string
      role: Role
      verified: boolean
      receiveEmail: boolean
      token: string | null
      groups: {
        groupId: string
        userId: string
        role: GroupRole
      }[]
    }
  }
  namespace App {
    interface Locals {
      auth: import('lucia').AuthRequest
      user: Lucia.DatabaseUserAttributes
      startTimer: number
      error: string
      errorId: string
      errorStackTrace: string
      message: unknown
      track: unknown
    }
  }
}

// THIS IS IMPORTANT!!!
export {}
