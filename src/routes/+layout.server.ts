import prisma from '$lib/config/prisma'

export const load = async (event: { locals: { user: any } }) => {
  const groups = await prisma.groupMember.findMany({
    where: {
      userId: event.locals.user?.userId,
    },
    include: {
      group: true,
    },
  })

  return { user: event.locals.user, groups }
}
