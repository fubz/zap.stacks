import { auth } from '$lib/server/lucia';
import { fail, redirect } from '@sveltejs/kit';

// https://lucia-auth.com/guidebook/sign-in-with-username-and-password/sveltekit/#sign-out-users
export const actions = {
	default: async ({ locals }) => {
		const session = await locals.auth.validate();
		if (!session) return fail(401);
		await auth.invalidateSession(session.sessionId); // invalidate session
		locals.auth.setSession(null); // remove cookie
		throw redirect(302, '/auth/sign-in');
	}
};
