import { auth } from '$lib/server/lucia'
import { fail, redirect, type ServerLoadEvent } from '@sveltejs/kit'
import prisma from '$lib/config/prisma.js'

// https://lucia-auth.com/guidebook/sign-in-with-username-and-password/sveltekit/#sign-out-users
export const actions = {
  default: async ({ locals }: ServerLoadEvent) => {
    const session = await locals.auth.validate()
    if (!session) return fail(401)

    const user = locals.user

    const userWithGroups = await prisma.authUser.findUnique({
      where: {
        id: user.userId,
      },
      include: {
        ownedGroups: {
          include: {
            group: true,
          },
        },
        memberGroups: {
          include: {
            group: true,
          },
        },
      },
    })
  },
}
