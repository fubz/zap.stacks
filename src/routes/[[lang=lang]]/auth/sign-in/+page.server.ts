import { fail, redirect } from '@sveltejs/kit'
import { setError, superValidate } from 'sveltekit-superforms/server'
import { auth, signin } from '$lib/server/lucia'
import { userSchema } from '$lib/config/zod-schemas'
import { LuciaError } from 'lucia'
import { PrismaClientValidationError } from '@prisma/client/runtime/library.js'
import prisma from '$lib/config/prisma.js'

const signInSchema = userSchema.pick({
  email: true,
  password: true,
})

export const load = async (event) => {
  const session = await event.locals.auth.validate()

  if (session) {
    if (!session.user.verified) {
      throw redirect(302, '/auth/verify/email')
    }
    throw redirect(302, '/dashboard')
  }
  const form = await superValidate(event, signInSchema)
  return {
    form,
  }
}

export const actions = {
  default: async (event) => {
    const form = await superValidate(event, signInSchema)
    //console.log(form);

    if (!form.valid) {
      return fail(400, {
        form,
      })
    }

    //add user to db
    try {
      const key = await signin({ ...form.data })

      const session = await auth.createSession({
        userId: key.userId,
        attributes: {},
      })
      event.locals.auth.setSession(session)
    } catch (e) {
      console.error(e)
      return setError(form, 'The email or password is incorrect.')
    }

    return { form }
  },
}
