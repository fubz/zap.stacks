import { fail, redirect } from '@sveltejs/kit'
import { setError, superValidate } from 'sveltekit-superforms/server'
import { createGroupSchema } from '$lib/config/zod-schemas'
import prisma from '$lib/config/prisma.js'

export const load = async (event) => {}

export const actions = {
  default: async (event) => {
    const user = event.locals.user
    if (!user) fail(401)
    const form = await superValidate(event, createGroupSchema)

    if (!form.valid) {
      return fail(400, {
        form,
      })
    }

    try {
      const createdGroup = await prisma.group.create({
        data: {
          name: form.data.groupName,
          members: {
            create: [{ userId: user.userId, role: 'OWNER' }],
          },
        },
        include: {
          members: true,
        },
      })

      const calendar = await prisma.calendar.create({
        data: { name: form.data.groupName, color: 'blue', groupId: createdGroup.id },
      })

      console.log('Create a Group: ', { createdGroup, calendar })
      return { form, createdGroup, calendar }
    } catch (e) {
      console.error(e)
      return setError(form, 'Failed to create a group.')
    }
  },
}
