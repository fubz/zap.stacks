import { fail, redirect } from '@sveltejs/kit'
import { setError, superValidate } from 'sveltekit-superforms/server'
import { joinGroupSchema, userSchema } from '$lib/config/zod-schemas'

export const load = async (event) => {}

export const actions = {
  default: async (event) => {
    const form = await superValidate(event, joinGroupSchema)
    console.log(form)

    if (!form.valid) {
      return fail(400, {
        form,
      })
    }

    //add user to db
    try {
      console.log('Join Group: ', form)
      throw new Error('boop beep')
    } catch (e) {
      console.error(e)
      return setError(form, 'Failed to join group.')
    }

    return { form }
  },
}
