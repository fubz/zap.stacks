import { fail, redirect } from '@sveltejs/kit'
import { setError, superValidate, message } from 'sveltekit-superforms/server'
import { auth } from '$lib/server/lucia'
import { userSchema } from '$lib/config/zod-schemas'
import { updateEmailAddressSuccessEmail } from '$lib/config/email-messages'

const profileSchema = userSchema.pick({
  firstName: true,
  lastName: true,
  email: true,
})

export const load = async (event) => {
  const form = await superValidate(event, profileSchema)
  const user = event.locals.user
  if (!user) throw redirect(302, '/auth/sign-in')

  form.data = {
    firstName: user.firstName,
    lastName: user.lastName,
    email: user.email,
  }
  return {
    form,
  }
}

export const actions = {
  default: async (event) => {
    const form = await superValidate(event, profileSchema)

    if (!form.valid) {
      return fail(400, {
        form,
      })
    }

    //add user to db
    try {
      const user = await event.locals.user
      console.log('updating profile', { user, form })

      auth.updateUserAttributes(user.userId, {
        firstName: form.data.firstName,
        lastName: form.data.lastName,
        email: form.data.email,
      })
      //await auth.invalidateAllUserSessions(user.id);

      if (user.email !== form.data.email) {
        const token = crypto.randomUUID()

        auth.updateUserAttributes(user.userId, {
          verified: false,
          token,
        })
        //await auth.invalidateAllUserSessions(user.id);
        await updateEmailAddressSuccessEmail(form.data.email, user.email, token)
      }
    } catch (e) {
      console.error(e)
      return setError(form, 'There was a problem updating your profile.')
    }
    console.log('profile updated successfully')
    return message(form, 'Profile updated successfully.')
  },
}
