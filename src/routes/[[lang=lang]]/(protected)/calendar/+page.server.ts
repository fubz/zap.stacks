export const ssr = false
import prisma from '$lib/config/prisma'
import { setError, superValidate } from 'sveltekit-superforms/server'
import type { Actions } from './$types.js'
import { calendarSchema } from '$lib/config/zod-schemas.js'
import { fail } from '@sveltejs/kit'

export async function load(event) {
  let userCalendars = await prisma.authUser.findUnique({
    where: {
      id: event.locals.user.userId,
    },
    select: {
      groups: {
        select: {
          group: {
            select: {
              Calendar: true,
            },
          },
        },
      },
    },
  })

  // Extract the calendars
  const calendars = !userCalendars ? [] : userCalendars.groups.flatMap((groupMember) => groupMember.group.Calendar)

  console.log({ calendars })

  return {
    calendars,
  }
}

export const actions: Actions = {
  createCalendar: async (event) => {
    const form = await superValidate(event, calendarSchema)
    //console.log(form);

    if (!form.valid) {
      return fail(400, {
        form,
      })
    }

    try {
      await prisma.calendar.create({ data: { name: form.data.name, color: 'green', groupId: '' } })
    } catch (error) {
      return setError(form, 'email', error || 'A user with that email already exists.')
    }
  },
}
