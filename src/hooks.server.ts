import { auth } from '$lib/server/lucia'
import { redirect, type Handle } from '@sveltejs/kit'
import type { HandleServerError } from '@sveltejs/kit'
import log from '$lib/server/log'
import { sourceLanguageTag, type AvailableLanguageTag } from '$lang/runtime'
import { getTextDirection } from '$lib/_helpers/i18n'

export const handleError: HandleServerError = async ({ error, event }) => {
  const errorId = crypto.randomUUID()

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  event.locals.error = error?.toString() || undefined
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  //@ts-ignore
  event.locals.errorStackTrace = error?.stack || undefined
  event.locals.errorId = errorId
  log(500, event)

  return {
    message: 'An unexpected error occurred.',
    errorId,
  }
}

export const handle: Handle = async ({ event, resolve }) => {
  // User Handling
  const startTimer = Date.now()
  event.locals.startTimer = startTimer

  event.locals.auth = auth.handleRequest(event)
  const session = await event.locals.auth.validate()

  if (session) {
    const user = session.user
    console.log('handle', { session })
    event.locals.user = user
    if (event.route.id?.startsWith('/(protected)')) {
      if (!user) throw redirect(302, '/auth/sign-in')
      if (!user.verified) throw redirect(302, '/auth/verify/email')
    }
  }

  // Language Handling
  const lang: AvailableLanguageTag = (event.params.lang as AvailableLanguageTag) ?? sourceLanguageTag
  const textDirection = getTextDirection(lang)

  const response = await resolve(event, {
    transformPageChunk({ done, html }) {
      if (done) {
        return html.replace('%lang%', lang).replace('%textDir%', textDirection)
      }
    },
  })
  log(response.status, event)
  return response
}
